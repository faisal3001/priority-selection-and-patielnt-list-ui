import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { TableModule } from 'primeng/table';
import { AppComponent } from './app.component';
import { CardComponent } from './card/card.component';
import { MinorSurgeryComponent } from './minor-surgery/minor-surgery.component';
import { CreateOperationComponent } from './create-operation/create-operation.component';
import { OperationComponent } from './operation/operation.component';
import { PostOperationComponent } from './post-operation/post-operation.component';
import { Step4Component } from './step4/step4.component';
import { FollowUpComponent } from './follow-up/follow-up.component';
import * as jQuery from 'jquery';


import 
{ 
  MenubarModule,
  RadioButtonModule,
  GrowlModule, 
  AccordionModule, 
  TabMenuModule,
  ScheduleModule,
  DropdownModule, 
  ButtonModule,
  InputTextModule, 
  DialogModule, 
  MenuModule, 
  StepsModule, 
  SharedModule, 
  CheckboxModule,
  OrderListModule, 
  DataScrollerModule, 
  TabViewModule, 
  TooltipModule, 
  ChipsModule,
  AutoCompleteModule,
  ConfirmDialogModule,
  ConfirmationService,
  DataTableModule,
  InputTextareaModule,
  Dropdown,
  PanelModule,
  ToolbarModule,
  SpinnerModule,
  ListboxModule,
  CalendarModule,
  GalleriaModule,
  TreeTableModule,
  FieldsetModule,
  FileUploadModule,
  SplitButtonModule,
  LightboxModule,

  ChartModule,
  SelectButtonModule
} from 'primeng/primeng';
import { ActiveSurgeryPatientsComponent } from './active-surgery-patients/active-surgery-patients.component';

@NgModule({
    declarations: [
        AppComponent,
        CardComponent,
        MinorSurgeryComponent,
        CreateOperationComponent,
        OperationComponent,
        PostOperationComponent,
        Step4Component,
        FollowUpComponent,
        ActiveSurgeryPatientsComponent
    ],
    imports: [
        BrowserModule,
        BrowserAnimationsModule,
        FormsModule,
        TableModule,
        HttpClientModule,
        InputTextModule,
        DialogModule,
        ButtonModule,
        CheckboxModule,
        TabViewModule,
        ToolbarModule,
        StepsModule,
        AutoCompleteModule,
        MenuModule,
        AccordionModule,
        PanelModule,
        SelectButtonModule,
        FieldsetModule,
        InputTextareaModule,
        CalendarModule,
        MenubarModule,
  LightboxModule,
  FileUploadModule,
  RadioButtonModule,
  GrowlModule, 
  SplitButtonModule,
  FieldsetModule,
  TreeTableModule,
  ScheduleModule,
  AccordionModule,
  ChartModule, 
  TabMenuModule,
  DropdownModule, 
  CalendarModule,
  ListboxModule,
  ButtonModule,
  InputTextModule, 
  DialogModule, 
  SpinnerModule,
  MenuModule, 
  StepsModule, 
  SharedModule,
  ToolbarModule, 
  CheckboxModule, 
  OrderListModule, 
  DataScrollerModule, 
  TabViewModule, 
  TooltipModule, 
  ChipsModule,
  GalleriaModule,
  AutoCompleteModule,
  ConfirmDialogModule,
  DataTableModule,
  InputTextareaModule,
  PanelModule

    ],
    providers: [],
    bootstrap: [AppComponent]
})
export class AppModule { }
