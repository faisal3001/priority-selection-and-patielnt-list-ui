import { Component, OnInit, Output } from '@angular/core';
import { Car } from './domain/car';
import { CarService } from './services/carservice';
import {MenuItem, SelectItem} from 'primeng/api';

export class PrimeCar implements Car {
    constructor(public vin?, public year?, public brand?, public color?) {}
}
export class MyEvent {
    id: number;
    title: string;
    start: string;
    end: string;
    allDay: boolean = true;
}
export class Patient {
    name:string;
    id:string;
    care:string;
    priority:string;
}
@Component({
    selector: 'app-root',
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.css'],
    providers: [CarService]
})
export class AppComponent implements OnInit {
    selectedCities: string[] = [];

    selectedCategories: string[] = ['Technology', 'Sports'];
    patientsList:Patient[];
    checked: boolean = false;
    index: number = 0;
    consualtantReview:boolean = false;
    diagnosticScreening:boolean = false;
    medication:boolean = false;
    surgery:boolean = false;
    dhs:boolean = false;
    tertiaryCare:boolean = false;
    activeIndex:number;
    
    types: SelectItem[];

    selectedType: string;

    callFunc(e){
        console.log("Active Index", e);
        this.activeIndex = e;
    }


    testFunc(e){
        console.log("Event" , e)
        if (e)
        this.index = 1;
        else
        this.index = 2;
    }
    displayDialog: boolean;

    car: Car = new PrimeCar();

    selectedCar: Car;

    newCar: boolean;

    cars: Car[];

    cols: any[];

    items: MenuItem[];

    selectedTypes: string[];
    types2: SelectItem[];
    constructor(private carService: CarService) { }

    ngOnInit() {
        this.types = [
            {label: 'P1', value: 'P1' },
            {label: 'P2', value: 'P2'},
            {label: 'P3', value: 'P3'},
            {label: 'P4', value: 'P4'}
        ];
        this.types2 = [
            {label: 'Consultation Review', value: 'Consultation Review', icon:'fa-bed' },
            {label: 'Diagnostic', value: 'Diagnostic', icon:'fa-flask'},
            {label: 'Medication', value: 'Medication', icon:'fa-heart'},
            {label: 'Minor Surgery', value: 'Minor Surgery', icon:'fa-wheelchair'},
            {label: 'Refer to DHS', value: 'Refer to DHS', icon:'fa-ambulance'},
            {label: 'Tertiary Care', value: 'Tertiary Care' , icon:'fa-heartbeat'}
        ];
        this.carService.getCarsSmall().then(cars => this.cars = cars);

        this.cols = [
            { field: 'vin', header: 'Vin' },
            { field: 'year', header: 'Year' },
            { field: 'brand', header: 'Brand' },
            { field: 'color', header: 'Color' }
        ];
        this.items = [{
            label: 'File',
            items: [
                {label: 'New', icon: 'fa fa-fw fa-plus'},
                {label: 'Open', icon: 'fa fa-fw fa-download'},
                {label: 'Undo', icon: 'fa fa-fw fa-refresh'},
                {label: 'Redo', icon: 'fa fa-fw fa-repeat'},
                {label: 'New', icon: 'fa fa-fw fa-plus'},
                {label: 'Open', icon: 'fa fa-fw fa-download'},
                
            ]
        }];
        this.patientsList = [
            {name: "Patient Patient1" , id: "123" , care:"Minor Surgery" , priority:"P1"},
            {name: "Patient Patient1" , id: "123" , care:"Minor Surgery",priority:"P1"},
            {name: "Patient Patient1" , id: "123" , care:"Minor Surgery",priority:"P1"},
            {name: "Patient Patient1" , id: "123" , care:"Minor Surgery",priority:"P1"},
            {name: "Patient Patient1" , id: "123" , care:"Minor Surgery",priority:"P3"},
            {name: "Patient Patient1" , id: "123" , care:"Minor Surgery",priority:"P1"},
            {name: "Patient Patient1" , id: "123" , care:"Minor Surgery",priority:"P2"},
            {name: "Patient Patient1" , id: "123" , care:"Minor Surgery",priority:"P3"},
            {name: "Patient Patient1" , id: "123" , care:"Minor Surgery",priority:"P2"},
            {name: "Patient Patient1" , id: "123" , care:"Minor Surgery",priority:"P4"},
            {name: "Patient Patient1" , id: "123" , care:"Minor Surgery",priority:"P3"},
            {name: "Patient Patient1" , id: "123" , care:"Minor Surgery",priority:"P4"},
        ];
        this.itemsSteps = [
            {label: 'Pre-operation'},
            {label: 'Operation'},
            {label: 'Post Operation'},
            {label: 'Step 4'},
            {label: 'Follow Up'}
        ];
        this.header = {
            left: 'prev,next today',
            center: 'title',
            right: 'month,agendaWeek,agendaDay'
        };
        
    }
    events: any[];

    header: any;
    showDialogToAdd() {
        this.newCar = true;
        this.car = new PrimeCar();
        this.displayDialog = true;
    }

    save() {
        const cars = [...this.cars];
        if (this.newCar) {
            cars.push(this.car);
        } else {
            cars[this.findSelectedCarIndex()] = this.car;
        }
        this.cars = cars;
        this.car = null;
        this.displayDialog = false;
    }

    delete() {
        const index = this.findSelectedCarIndex();
        this.cars = this.cars.filter((val, i) => i !== index);
        this.car = null;
        this.displayDialog = false;
    }

    onRowSelect(event) {
        this.newCar = false;
        this.car = {...event.data};
        this.displayDialog = true;
    }

    findSelectedCarIndex(): number {
        return this.cars.indexOf(this.selectedCar);
    }




    country: any;

    countries: any[];

    filteredCountriesSingle: any[];

    filteredCountriesMultiple: any[];

    patients: string[] = ['Faisal','Toni Kroos','Julian Brandt','Mats Hummels','Siqlain','Naveed','Ronaldo','Messi','James'];

    filteredPatients: any[];

    patient: string;

    filterCountry(query, countries: any[]):any[] {
        //in a real application, make a request to a remote url with the query and return filtered results, for demo we filter at client side
        let filtered : any[] = [];
        for(let i = 0; i < countries.length; i++) {
            let country = countries[i];
            if(country.name.toLowerCase().indexOf(query.toLowerCase()) == 0) {
                filtered.push(country);
            }
        }
        return filtered;
    }

    filterPatients(event) {
        this.filteredPatients = [];
        for(let i = 0; i < this.patients.length; i++) {
            let patient = this.patients[i];
            if(patient.toLowerCase().indexOf(event.query.toLowerCase()) == 0) {
                this.filteredPatients.push(patient);
            }
        }
    }

    pageNum:number = 7;
    prevFunc(){
        this.pageNum -- ;
        if (this.pageNum < 1)
            this.pageNum = 5;
    }
    nextFunc(){
        this.pageNum ++ ; 
        if (this.pageNum > 7)
            this.pageNum = 1;
    }
    itemsSteps: MenuItem[];
        
    activeIndexSteps: number = 0;

}
