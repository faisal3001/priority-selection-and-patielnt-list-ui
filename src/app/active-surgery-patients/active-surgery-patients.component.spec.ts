import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ActiveSurgeryPatientsComponent } from './active-surgery-patients.component';

describe('ActiveSurgeryPatientsComponent', () => {
  let component: ActiveSurgeryPatientsComponent;
  let fixture: ComponentFixture<ActiveSurgeryPatientsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ActiveSurgeryPatientsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ActiveSurgeryPatientsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
