import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MinorSurgeryComponent } from './minor-surgery.component';

describe('MinorSurgeryComponent', () => {
  let component: MinorSurgeryComponent;
  let fixture: ComponentFixture<MinorSurgeryComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MinorSurgeryComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MinorSurgeryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
